EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:74xgxx
LIBS:ac-dc
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:elec-unifil
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:logo
LIBS:microchip1
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:msp430
LIBS:nxp_armmcu
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:ucode_1-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title ""
Date "25 jun 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PIC12C509A PCA1
U 1 1 5769965B
P 5600 3250
F 0 "PCA1" H 5550 2550 60  0000 C CNN
F 1 "PCA9306" H 5600 4000 60  0000 C CNN
F 2 "~" H 5600 3250 60  0000 C CNN
F 3 "~" H 5600 3250 60  0000 C CNN
	1    5600 3250
	1    0    0    -1  
$EndComp
Text HLabel 4750 3750 0    60   Input ~ 0
SDA1
Text HLabel 4750 3450 0    60   Input ~ 0
SCL1
Text HLabel 6400 3750 2    60   Input ~ 0
SDA2
Text HLabel 6400 3450 2    60   Input ~ 0
SCL2
Text HLabel 4750 3050 0    60   Input ~ 0
VREF1
Text HLabel 6700 2400 2    60   Input ~ 0
VREF2
$Comp
L GND #PWR33
U 1 1 576996B6
P 4800 2800
F 0 "#PWR33" H 4800 2800 30  0001 C CNN
F 1 "GND" H 4800 2730 30  0001 C CNN
F 2 "" H 4800 2800 60  0000 C CNN
F 3 "" H 4800 2800 60  0000 C CNN
	1    4800 2800
	1    0    0    -1  
$EndComp
$Comp
L R R18
U 1 1 576996C7
P 6550 2650
F 0 "R18" V 6630 2650 40  0000 C CNN
F 1 "200K" V 6557 2651 40  0000 C CNN
F 2 "~" V 6480 2650 30  0000 C CNN
F 3 "~" H 6550 2650 30  0000 C CNN
	1    6550 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 3050 6300 2750
Wire Wire Line
	6550 2900 6300 2900
Connection ~ 6300 2900
Wire Wire Line
	6550 2400 6700 2400
Wire Wire Line
	6300 3450 6400 3450
Wire Wire Line
	6400 3750 6300 3750
Wire Wire Line
	4750 3450 4850 3450
Wire Wire Line
	4750 3750 4850 3750
Wire Wire Line
	4750 3050 4850 3050
Wire Wire Line
	4850 2750 4800 2750
Wire Wire Line
	4800 2750 4800 2800
$EndSCHEMATC
