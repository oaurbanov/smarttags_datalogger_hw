# README #

This repo contains the projects for the schematics and PCB designs of the UCODE-DataLogger.

QG8_v1 : first prototype, for testing I2C communications with UCODE chip.

L092_v2: uses the MSP430L092 from Texas Instruments, has errors reading from EEPROM.

QG8_v2: second prototype, completely working, but has some consumption issues. 1st Validation test run with this prototype.

### How do I get set up? ###

Just start the projects on KiCad

### Who do I talk to? ###

* Oscar Urbano oaurbanov@unal.edu.co