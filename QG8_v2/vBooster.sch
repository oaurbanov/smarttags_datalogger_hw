EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:74xgxx
LIBS:ac-dc
LIBS:brooktre
LIBS:cmos_ieee
LIBS:dc-dc
LIBS:elec-unifil
LIBS:ftdi
LIBS:gennum
LIBS:graphic
LIBS:hc11
LIBS:logo
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:msp430
LIBS:nxp_armmcu
LIBS:powerint
LIBS:pspice
LIBS:references
LIBS:relays
LIBS:rfcom
LIBS:sensors
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:transf
LIBS:ttl_ieee
LIBS:video
LIBS:smarttags
LIBS:smarttag-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date "19 aug 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TPS61221 TPS1
U 1 1 57671CC9
P 4900 2300
AR Path="/57670A4C/57671CC9" Ref="TPS1"  Part="1" 
AR Path="/5767A2E4/57671CC9" Ref="TPS2"  Part="1" 
AR Path="/5768D20D/57671CC9" Ref="TPS3"  Part="1" 
F 0 "TPS2" H 4900 1900 60  0000 C CNN
F 1 "TPS61221" H 4900 2700 60  0000 C CNN
F 2 "smarttags:TPS61221" H 4900 2300 60  0001 C CNN
F 3 "" H 4900 2300 60  0000 C CNN
	1    4900 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 2300 4350 2400
Wire Wire Line
	4350 2400 5450 2400
Wire Wire Line
	5450 2400 5450 2500
Wire Wire Line
	4350 2100 4350 2200
Wire Wire Line
	4350 2200 6300 2200
$Comp
L INDUCTOR L1
U 1 1 57671D16
P 6000 2300
AR Path="/57670A4C/57671D16" Ref="L1"  Part="1" 
AR Path="/5767A2E4/57671D16" Ref="L1"  Part="1" 
AR Path="/5768D20D/57671D16" Ref="L3"  Part="1" 
AR Path="/57671D16" Ref="L1"  Part="1" 
F 0 "L1" H 5850 2250 40  0000 C CNN
F 1 "4.7 uH" H 6100 2250 40  0000 C CNN
F 2 "" H 6000 2300 60  0001 C CNN
F 3 "~" H 6000 2300 60  0000 C CNN
	1    6000 2300
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 57671D46
P 4050 2350
AR Path="/57670A4C/57671D46" Ref="C2"  Part="1" 
AR Path="/5767A2E4/57671D46" Ref="C2"  Part="1" 
AR Path="/5768D20D/57671D46" Ref="C12"  Part="1" 
AR Path="/57671D46" Ref="C2"  Part="1" 
F 0 "C2" H 4050 2450 40  0000 L CNN
F 1 "10 uf" H 4056 2265 40  0000 L CNN
F 2 "" H 4088 2200 30  0001 C CNN
F 3 "~" H 4050 2350 60  0000 C CNN
	1    4050 2350
	1    0    0    -1  
$EndComp
$Comp
L C C3
U 1 1 57671D55
P 5850 2700
AR Path="/57670A4C/57671D55" Ref="C3"  Part="1" 
AR Path="/5767A2E4/57671D55" Ref="C3"  Part="1" 
AR Path="/5768D20D/57671D55" Ref="C13"  Part="1" 
AR Path="/57671D55" Ref="C3"  Part="1" 
F 0 "C3" H 5850 2800 40  0000 L CNN
F 1 "10 uF" H 5856 2615 40  0000 L CNN
F 2 "" H 5888 2550 30  0001 C CNN
F 3 "~" H 5850 2700 60  0000 C CNN
	1    5850 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2150 4350 2150
Connection ~ 4350 2150
Wire Wire Line
	5450 2500 6150 2500
Wire Wire Line
	4050 2500 4050 2600
Wire Wire Line
	5850 2850 5850 2950
Text HLabel 3850 2150 0    60   Input ~ 0
VIN
Text HLabel 6150 2500 2    60   Input ~ 0
VOUT
Connection ~ 4050 2150
Connection ~ 5850 2500
Text HLabel 5550 2100 2    60   Input ~ 0
EN
Wire Wire Line
	5550 2100 5450 2100
Wire Wire Line
	6300 2200 6300 2300
Wire Wire Line
	6300 2300 6250 2300
Wire Wire Line
	4050 2200 4050 2150
Wire Wire Line
	5850 2550 5850 2500
Wire Wire Line
	5450 2300 5750 2300
Wire Wire Line
	4350 2500 4350 2550
$Comp
L GND #PWR?
U 1 1 5A3AF17F
P 4050 2600
F 0 "#PWR?" H 4050 2350 50  0001 C CNN
F 1 "GND" H 4050 2450 50  0000 C CNN
F 2 "" H 4050 2600 50  0000 C CNN
F 3 "" H 4050 2600 50  0000 C CNN
	1    4050 2600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A3AF1C2
P 4350 2550
F 0 "#PWR?" H 4350 2300 50  0001 C CNN
F 1 "GND" H 4350 2400 50  0000 C CNN
F 2 "" H 4350 2550 50  0000 C CNN
F 3 "" H 4350 2550 50  0000 C CNN
	1    4350 2550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR?
U 1 1 5A3AF1D9
P 5850 2950
F 0 "#PWR?" H 5850 2700 50  0001 C CNN
F 1 "GND" H 5850 2800 50  0000 C CNN
F 2 "" H 5850 2950 50  0000 C CNN
F 3 "" H 5850 2950 50  0000 C CNN
	1    5850 2950
	1    0    0    -1  
$EndComp
$EndSCHEMATC
